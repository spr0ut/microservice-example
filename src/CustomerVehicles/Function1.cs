using System;
using System.IO;
using System.Threading.Tasks;
using MicroService.Core.Models;
using MicroService.Core.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CustomerVehicles
{
    public class Function1
    {
        private readonly ITableStorage<CustomerVehicleEntity> _vehicleData;

        public Function1(ITableStorage<CustomerVehicleEntity> vehicleData)
        {
            _vehicleData = vehicleData;
        }

        [FunctionName("CustomerVehicles")]
        public async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "customers/{customerID}")] HttpRequest req, string customerID, ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var vehicles = _vehicleData.GetEntitiesByPartitionKey(customerID);
            //string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            //dynamic data = JsonConvert.DeserializeObject(requestBody);

            return new OkObjectResult(vehicles);
        }
    }
}