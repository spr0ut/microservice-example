﻿using System;
using MicroService.Core.Models;
using MicroService.Core.Storage;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;

[assembly: FunctionsStartup(typeof(CustomerVehicles.Startup))]

namespace CustomerVehicles
{
    public class Startup : FunctionsStartup
    {
        //private readonly static string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        private readonly static string storageConnectionString = Environment.GetEnvironmentVariable("Storage");

        private const string CUSTOMER_VEHICLES = "customervehicles";

        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton<ITableStorage<CustomerVehicleEntity>>(_ => new TableStorage<CustomerVehicleEntity>(CUSTOMER_VEHICLES, storageConnectionString));
        }
    }
}